# -*- coding: utf-8 -*-
"""
Created on Sat May 14 09:20:38 2022

@author: Daniel Zapata

Punto 4 - Manejo Secuencias
"""

def ContarCaracter(texto, caracter):
    contador = 0
    for i in texto:
        if i == caracter:
            contador = contador + 1
    
    return contador

texto = input('Ingrese el texto: \n')
letraBuscar=input('Ingrese la letra a buscar en el texto ingresado, para saber la cantidad de veces que se repite:   ')

numeroVeces = ContarCaracter(texto, letraBuscar)

print("El número de veces que aparece la letra ", letraBuscar, ' es: ', numeroVeces)
