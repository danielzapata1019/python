# -*- coding: utf-8 -*-
"""
Created on Sat May 14 08:31:29 2022

@author: Daniel Zapata

Punto 2 -  Manejo Archivos
"""

import pandas as pd

datos=pd.read_csv('techogar.csv',header=0)

def ObtenerPromedioColumnaXUltimosNAnios(anios, columna):
    print ("\n")
    print("Promedio valor",columna," en los últimos ", anios," años \n")
    anioMayor = (datos.iloc[datos['Año'].idxmax()])["Año"]
    columnaUltimosAnios = datos.loc[(datos['Año'] > (anioMayor-anios)),[columna]]
    print(columnaUltimosAnios.mean())
    
    
def ObtenerMayorValorParaXColumna(columna):   
    print (columna,": Su mayor valor es: \n")
    print(datos.iloc[datos[columna].idxmax()][columna])


ObtenerMayorValorParaXColumna("Televisión")
ObtenerPromedioColumnaXUltimosNAnios(5,'Internet')