# -*- coding: utf-8 -*-
"""
Created on Sat May 14 08:17:30 2022

@author: Daniel Zapata

Primer punto evaluación final -  Validar correo
"""

import tkinter as tk
from tkinter import messagebox
import re 
  


def ValidarCorreo():
    if(re.search(regex,txtCorreo.get())):  
        lblMensaje.config(text = 'Correo Valido')
    else:  
        messagebox.showwarning('Advertencia','Correo electrónico no Valido')

        
regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    
ventana=tk.Tk()
ventana.title('Validar correo en Python')
ventana.geometry('380x300')
ventana.configure(bg='#DCDFE3')
ventana.iconbitmap('Python.ico')
ventana.resizable(0,0)

e1=tk.Label(ventana,text='Correo: ', bg='#DCDFE3', fg='black')
e1.pack(padx=10, pady=50,ipadx=10, ipady=10)

txtCorreo=tk.Entry(ventana)
txtCorreo.place(x=100, y=100, width=200, heigh=20)

lblMensaje = tk.Label(ventana, bg='#DCDFE3', fg='darkgreen')
lblMensaje.place(x=150, y=250)

boton=tk.Button(ventana, text='Validar Correo', 
                bg='darkblue', fg='white', command=ValidarCorreo)
boton.pack()
ventana.mainloop()