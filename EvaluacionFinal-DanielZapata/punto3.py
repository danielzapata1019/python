# -*- coding: utf-8 -*-
"""
Created on Sat May 14 08:46:05 2022

@author: Daniel Zapata

Punto 3: Programación Orientada a Objetos
"""
class OperacionBasica():
    def __init__(self, numero1, numero2):
        self.numero1=numero1
        self.numero2=numero2
        self.resultado=0
        
    def Sumar(self):
        self.resultado=self.numero1 + self.numero2
        print('Resultado Suma: ', self.resultado)
        
    def Restar(self):
        self.resultado=self.numero1 - self.numero2
        print('Resultado Resta: ', self.resultado)
    
    def Multiplicar(self):
        self.resultado=self.numero1 * self.numero2
        print('Resultado Multiplica: ', self.resultado)
    
    def Dividir(self):
        if self.numero2 == 0:
            print("No se permite dividir por 0")
        else:
            self.resultado=self.numero1 / self.numero2
            print('Resultado Dividir: ', self.resultado)
            
    def DivisionEntera(self):
        if self.numero2 == 0:
            print("No se permite dividir por 0")
        else:
            self.resultado=self.numero1 // self.numero2
            print('Resultado División Entera: ', self.resultado)
            
    def Modulo(self):
        self.resultado=self.numero1 % self.numero2
        print('Resultado Modulo: ', self.resultado)
        
suma=OperacionBasica(4, 10)
suma.Sumar()
        
resta=OperacionBasica(10, 3)
resta.Restar()

multiplica=OperacionBasica(5, 3)
multiplica.Multiplicar()

divide=OperacionBasica(5, 2)
divide.Dividir()

divisionEntera=OperacionBasica(15, 2)
divisionEntera.DivisionEntera()

modulo=OperacionBasica(15, 2)
modulo.Modulo()