"""
Created on Mon Mar 28 21:26:13 2022

@author: Daniel Zapata
"""
import numpy as np
import sys

def CrearDiccionario():
    diccionario={}    
    continuar='si'
    
    while continuar=='si':
        clave= input('Digite Nombre candidato: \n')        
        valor= np.random.randint(0,1000,(5,6))
        diccionario.update({clave:valor})
        print('digite "si" para seguir ingresando pares , o cualquier tecla para terminar \n')
        continuar=input('')
    return diccionario

def ConsultasCandidato(tabla,opc): 
    respuesta = None  
     
    if opc == 7:
        sys.exit()
    
    nombre = input('Digite el nombre del candidato \n')
    clave = nombre  
    
    if opc == 1:        
        respuesta = ConsultarVotosCandidato(clave)
    elif opc == 2:
        respuesta =  AnalizarVotosEstratosCandidato(clave)
    elif opc == 3:
        respuesta =  AnalizarVotosCiudadesCandidato(clave)
    elif opc == 4:
        respuesta =  ObtenerMayorVotosCiudad(clave)
    elif opc == 5:
        respuesta =  ObtenerMayorVotosEstrato(clave)
    elif opc == 6:
        respuesta =  ObtenerMenorVotosEstrato(clave)
    return respuesta

def BuscarCandidato(tabla, candidato):
    votos={}
    for i in tabla:
        if candidato in tabla:
            votos=tabla[candidato]    
    return votos

def ConsultarVotosCandidato(nombre):
    diccionario={}    
    diccionario = BuscarCandidato(tablaVotaciones, nombre)
    return diccionario

def AnalizarVotosEstratosCandidato(clave):
    matriz=None
    respuestaVotosEstratos=''
    matriz = BuscarCandidato(tablaVotaciones, clave)    
    sumaEstratos = np.sum(matriz, axis=0)    
    x = len(sumaEstratos)
    for i in range(0,x):            
        respuestaVotosEstratos = respuestaVotosEstratos + VectorEstratos[i] + ':' + str(sumaEstratos[i]) + '\n'
    return respuestaVotosEstratos

def AnalizarVotosCiudadesCandidato(clave):
    matriz=None
    respuestaVotosCiudades=''
    matriz = BuscarCandidato(tablaVotaciones, clave)    
    sumaCiudades = np.sum(matriz, axis=1)    
    x = len(sumaCiudades)
    for i in range(0,x):            
        respuestaVotosCiudades = respuestaVotosCiudades + VectorCiudades[i] + ':' + str(sumaCiudades[i]) + '\n'
    return respuestaVotosCiudades    

def ObtenerMayorVotosCiudad(clave):
    maximo=None
    matriz = BuscarCandidato(tablaVotaciones, clave)    
    sumaCiudades = np.sum(matriz, axis=1)    
    maximo = sumaCiudades.max()
    result = np.where(sumaCiudades == np.amax(sumaCiudades))
    posicion = result[0]
    respuesta = 'La ciudad con mayor votación es: ' + VectorCiudades[posicion[0]] + ' con ' + str(maximo) + ' Votos'
    return respuesta

def ObtenerMayorVotosEstrato(clave):
    maximo=None
    matriz = BuscarCandidato(tablaVotaciones, clave)    
    sumaEstratos = np.sum(matriz, axis=0)    
    maximo = sumaEstratos.max()
    result = np.where(sumaEstratos == np.amax(sumaEstratos))
    posicion = result[0]
    respuesta = 'El estrato con mayor votación es: ' + VectorEstratos[posicion[0]] + ' con ' + str(maximo) + ' Votos'
    return respuesta

def ObtenerMenorVotosEstrato(clave):
    minimo=None
    matriz = BuscarCandidato(tablaVotaciones, clave)    
    sumaEstratos = np.sum(matriz, axis=0)    
    minimo = np.amin(sumaEstratos)
    result = np.where(sumaEstratos == np.amin(sumaEstratos))
    posicion = result[0]
    respuesta = 'El estrato al cual se debe mejorar estrategia de campaña es: ' + VectorEstratos[posicion[0]] + ' actualmente con ' + str(minimo) + ' Votos'
    return respuesta

def MostrarMenu():
    
    opcion= int(input("""Digite opción de consulta:\n 
    1. Consulta Votos Candidato \n 
    2. Análisis votos por estrato para un candidato \n
    3. Análisis votos por ciudad para un candidato \n
    4. Ciudad con mayor votos para un candidato \n
    5. Estrato con mayor votos para un candidato \n
    6. Estrato en el que se debe mejorar estrategia de campaña \n
    7. Fin \n"""))
    
    resp = ConsultasCandidato(tablaVotaciones,opcion)
    print(resp) 
    MostrarMenu()

#Programa principal
VectorEstratos = ['Estrato 1','Estrato 2', 'Estrato 3', 'Estrato 4','Estrato 5', 'Estrato 6']
VectorCiudades = ['MED','BOG', 'CTG', 'STM','CLO']
tablaVotaciones= CrearDiccionario()
print('*** Resultados Votaciones ***')
print(tablaVotaciones)
MostrarMenu()
