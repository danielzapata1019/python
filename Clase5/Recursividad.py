# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 11:23:48 2022

@author: Daniel Zapata
"""

#Recursividad

from timeit import default_timer

def CuentaRegresiva(num):
    num-=1
    if num>0:
        print(num)
        CuentaRegresiva(num)
    else:
        print('Fin')
        


#ppal

inicio = default_timer()
valor=int(input('Digita un entero: \n'))
CuentaRegresiva(valor)
fin=default_timer()

print('El tiempo de ejecución fue de ', fin-inicio, ' milisegundos.')
