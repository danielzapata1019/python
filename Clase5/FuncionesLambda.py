# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 11:34:19 2022

@author: Daniel Zapata
"""

import math

r=lambda a:math.sqrt(a)
R=r(3)
print(R)


f=lambda x,y:x+y
print(f(2,3))

D=lambda a,b,c: b**2-4*a*c
print(D(1,3,5))