# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 11:11:18 2022

@author: Daniel Zapata
"""

#Funciones de Orden superior

#Funciones que tienen dentro de ellas otras funciones

def Operar(v1, v2, funcion):
    return funcion(v1,v2)
    
def Sumar(v1,v2):
    return v1+v2

def Restar(v1,v2):
    return v1-v2

def Multiplicar(v1,v2):
    return v1*v2

def Dividir(v1,v2):
    return v1/v2
    
    
print(Operar(50, 5, Sumar))
print(Operar(50, 5, Restar))
print(Operar(50, 5, Multiplicar))
print(Operar(50, 5, Dividir))