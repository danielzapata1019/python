# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 11:32:20 2022

@author: Daniel Zapata
"""


def factorial(n, depth = 1):
    if n == 1:
        print('\t' * depth, 'Retornando 1')
        return 1
    else:
        print('\t'*depth,'Llamando recurrentemente factorial(',n-1,')')
        result = n * factorial(n-1, depth + 1)
        print('\t' * depth, 'Retornando:', result)
        return result
    
print('llamando función factorial( 5 )')
print(factorial(5))