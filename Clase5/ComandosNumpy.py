# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 11:09:52 2022

@author: Daniel Zapata
"""
#Comandos Numpy

import numpy as np

D=np.array([(1,2,3),(4,5,6)])
print('D=  ',D)

D1=np.matrix([[1,2,3],[4,5,6]])
print('D1=  ',D1)

U=np.ones((4,5))
print('U=  ',U)

Z=np.zeros((4,5))
print('Z=  ',Z)

V=np.empty((3,3))
print('V=  ',V)

A=np.random.random((3,3))
print('A=  ',A)

#A1=random_matrix = numpy.random.randint(min_val,max_val,(<num_rows>,<num_cols>))
A1=np.random.randint(10,20,(3,3))

print('A1= ',A1)

C=np.full((4,5),5)
print('C= ',C)

#creo valores entre 10 y <80 con incremento 10
ME=np.arange(10,80,10)
print('ME=  ',ME)

#creo un vector con 6 valores entre 0 y 2
ME1=np.linspace(0,2,6)
print('ME1=  ',ME1)

#creo una matriz identidad de orden 4
I=np.eye(4,4)
print('I=  ',I)

#creo una matriz identidad de orden 4
I1=np.identity(4)
print('I1=  ',I1)

D=np.array([(1,2,3),(4,5,6)])
print(D.ndim)

D=np.array([(1,2,3),(4,5,6)])
print(D.dtype)

#encontrar tamaño de la matriz
print(D.size)

#encontrar dimensión de la matriz
print(D.shape)

#cambiar la disposición de la matriz
D2=D.reshape(3,2)
print('D2=  ',D2)

#selecionar un solo elemento de la matriz
print(D[1,2])

#obtener todos los elementos de la columna 2
print(D[0:,2])

#obtener el elemento mínimo de la matriz
print(D.min())

#obtener el elemento máximo de la matriz
print(D.max())

#obtener la suma de elementos de la matriz
print(D.sum())

#obtener raíz cuadrada de los elementos de la matriz
print(np.sqrt(D))

#obtener desviación estandar de los elementos de la matriz
print(np.std(D))

#operaciones con matrices
E=np.random.random((3,3))
F=np.random.random((3,3))
print('E=  ',E)
print('F=  ',F)
#suma
print(E+F)
#resta
print(E-F)
#multiplicación
print(E*F)
#división
print(E/F)

G=np.arange(36)
print('G= ',G)
H=G.reshape(6,6)
print('H=  ',H)
  #redimensionando
J=np.arange(5)
print('J=  ',J)
J.resize((8,))
print('J=',J)