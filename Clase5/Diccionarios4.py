# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 10:16:25 2022

@author: Daniel Zapata
"""
import random
import numpy as np


def CrearDiccionario():
    diccionario={}    
    continuar='si'
    
    while continuar=='si':
        clave= input('Digite Nombre candidato: \n')
        valor=[[None for i in range(5)] for j in range(6)]
        valor=[[random.randint(0,100) for i in range(5)] for j in range(6)]
        diccionario.update({clave:valor})
        print('digite "si" para seguir ingresando pares , "no" para terminar \n')
        continuar=input('')
    return diccionario


def BuscarCandidato(libreta, cliente):
    numero=''
    for i in libreta:
        if cliente in libreta:
            numero=libreta[cliente]    
    return numero

libreta= CrearDiccionario()
print(libreta)

cliente=input('Digite el nombre del candidato \n')
telefono=BuscarCandidato(libreta,cliente)
print('El número de votos del candidato es: ', telefono)