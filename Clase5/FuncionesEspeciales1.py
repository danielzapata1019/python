# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 11:45:35 2022

@author: Daniel Zapata
"""

def ListaInfinita():
    i=1
    while True:
        yield i
        i=i+1
        
#Ppal
result=ListaInfinita()
print(next(result))
print(next(result))