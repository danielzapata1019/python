# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 09:04:05 2022

@author: Daniel Zapata
"""

def CrearDiccionario():
    diccionario={}    
    continuar='si'
    
    while continuar=='si':
        clave= input('Digite Nombre: \n')
        valor=input('Ingrese número teléfono cliente: \n')
        diccionario.update({clave:valor})
        print('digite "si" para seguir ingresando pares , "no" para terminar \n')
        continuar=input('')
    return diccionario

def BuscarTelefono(libreta, cliente):
    numero=''
    for i in libreta:
        if cliente in libreta:
            numero=libreta[cliente]    
    return numero

def CambiarValor(libreta):
    nuevoValor = input('digite el nuevo valor: \n')
    clave=input('digita la clave a la que quieres modificar su valor \n')
    libreta[clave]=nuevoValor
    
    return libreta

def BorrarValor(libreta):
    nuevoValor=None
    clave=input('digita la clave a la que quieres borrar su valor \n')
    libreta[clave]=nuevoValor
    
    return libreta

libreta= CrearDiccionario()
print(libreta)

cliente=input('Digite el nombre del cliente \n')
telefono=BuscarTelefono(libreta,cliente)
print('El número telefónico del cliente es: ', telefono)

'''
A=CambiarValor(libreta)
print(A)
B=BorrarValor(libreta)
print(B)

'''