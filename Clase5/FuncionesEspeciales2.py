# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 11:52:43 2022

@author: Daniel Zapata
"""

#PROGRAMA PARA ASIGNAR CLAVES A USUARIOS
import random

def generador_claves():
    clave=random.randint(1,10000)*1111111
    yield clave
    
    
usuarios=input('ingrese nombre de usuario: ')

while usuarios !='0':
    clave_asignada=next(generador_claves())
    print('la clave asignada para el usuario ',usuarios, '\nes ',clave_asignada)
    usuarios=input('ingrese nombre de usuario o cero para salir: ')

print('Gracias por utilizar nuestra aplicación!!!')