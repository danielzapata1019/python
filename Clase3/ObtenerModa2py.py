# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 10:40:44 2022

@author: Daniel Zapata
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 10:19:20 2022

@author: Daniel Zapata
"""
import random
import statistics
import math

print('programa para generar una matriz y encontrar la moda')
#programa principal
n=int(input('Ingrese el orden de la matriz: \n'))

#se ingresa el orden y se reserva espacio de memoria con ceros
I=[]
for i in range(n):
    I.append([0]*n)
    
#Se genera la matriz con datos aleatorios
for i in range(n):
    for j in range(n):
        I[i][j]= random.randint(10, 100)
               
#invoco la funciíon

print('La matriz ingresada fue: \n')
for i in I:
    print(i)

#moda=statistics.mode(I)
#print('El dato más repetido es: ', moda)

from scipy import stats
moda= stats.mode(I)

print('El dato más repetido es: ', moda)