https://bitbucket.org/danielzapata1019/python/src/main/Clase3/Elecciones.py




# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 10:56:47 2022

@author: Daniel Zapata
"""
import random
#Programa elecciones


def VotosPorCandidatos(A):
    
    for i in range(0,7):
        sumaFila=0
        for j in range(0,5):
            sumaFila  = sumaFila + A[i][j]        
        VC[i]= sumaFila
        
    return VC

def CiudadVotosBlanco(A):
    maximo = 0
    posicion = 0
    for j in range(5):
        if(A[6][j] > maximo):
            maximo = A[6][j]
            posicion = j
    return posicion

def ProcentajeBlanco(A):
    
    sumaTotalVotos=0
    for i in range(0,7):        
        for j in range(0,5):
            sumaTotalVotos  = sumaTotalVotos + A[i][j]        
         
        porcentajeVotosBlanco = (VC[6]/sumaTotalVotos)*100
        
    return porcentajeVotosBlanco


#se ingresa el orden y se reserva espacio de memoria con ceros
I=[]
VC=[None]*7
for i in range(0,7):
    I.append([0]*5)
    
#Se genera la matriz con datos aleatorios
for i in range(0,7):
    for j in range(0,5):
        I[i][j]= random.randint(0, 99)
               
#invoco la funciíon

print('La matriz ingresada fue: \n')
for i in I:
    print(i)
  
    
VectorCandidatos = ['Petro', 'Fico', 'Gutierrez', 'Galan', 'Carolina','Luis Peluca','Blanco']
VectorCiudades = ['Medellín','Bogotá', 'Cali', 'Cartagena','Pasto']


VC = VotosPorCandidatos(I)

print("############################")
print("Candidatos", VectorCandidatos)
print("Ciudades", VectorCiudades)

print('La cantidad de votos por candidato es:')
x = len(VectorCandidatos)
for i in range(x):
    print(VectorCandidatos[i], ": ", VC[i])

VB= CiudadVotosBlanco(I)
print('La ciudad con mayor votos en blanco es: ', VectorCiudades[VB])

PVB= ProcentajeBlanco(I)
print('La participacion del voto en blanco es: ', PVB, '%')

p=VC.index(max(VC))
print('el candidato con mayor opción es: ', VectorCandidatos[p])
