# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 10:19:20 2022

@author: Daniel Zapata
"""
import random

def ObtenerModa(A, n):
    contModa=0
    for i in range(n):
        for j in range(n):
            dato=A[i][j]
            contador=0
            for k in range(n):
                for l in range(n):
                    if dato == A[k][l]:
                        contador =  contador+1
            
            if contador > contModa:
                contModa= contador
                moda = dato
                
    if contModa==1:
        moda=None
        
    return moda


print('programa para generar una matriz y encontrar la moda')
#programa principal
n=int(input('Ingrese el orden de la matriz: \n'))

#se ingresa el orden y se reserva espacio de memoria con ceros
I=[]
for i in range(n):
    I.append([0]*n)
    
#Se genera la matriz con datos aleatorios
for i in range(n):
    for j in range(n):
        I[i][j]= random.randint(10, 100)
               
#invoco la funciíon

print('La matriz ingresada fue: \n')
for i in I:
    print(i)

moda=ObtenerModa(I, n)
print('El dato más repetido es: ', moda)
