# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 20:56:52 2022

@author: Daniel Zapata
"""
import random
def CrearMatriz(n,m):
    A=[]
    for i in range(0,n):
        A.append([0]*m)
    for i in range(0,n):
        for j in range(0,m):
            A[i][j]=random.randint(0, 100)
    return A


#Programa ppal

#filas=int(input("Digite Número de filas: "))
#columnas=int(input("Digite Número de Columnas: "))

filas=random.randint(1, 5)
columnas=random.randint(1, 5)

print("El número de filas es: ", filas, " y número de columnas es: ", columnas)

matriz=CrearMatriz(filas, columnas)

#print(matriz)

for i in matriz:
    print(i)