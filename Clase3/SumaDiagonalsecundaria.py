# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 08:13:12 2022

@author: Daniel Zapata
"""

import random

print('suma de los elementos de la diagonal secundaria')

def SumaDiagonalSecundaria(M,n):
    suma = 0 
    for i in range(n):
        for j in range(n):        
            if i+j==n-1:    
                suma =  suma + M[i][j]
    return suma


#programa principal
n=int(input('Ingrese el orden de la matriz: \n'))

#se ingresa el orden y se reserva espacio de memoria con ceros
I=[]
for i in range(n):
    I.append([0]*n)
    
#Se genera la matriz con datos aleatorios
for i in range(n):
    for j in range(n):
        I[i][j]= random.randint(0, 10)
               
#invoco la funciíon
sumadiagonal= SumaDiagonalSecundaria(I,n)

print('La suma de la diagonal secundaria es: ', sumadiagonal)
for i in I:
    print(i)