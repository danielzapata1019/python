# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 23:20:07 2022

@author: Daniel Zapata
"""


A=[]
for i in range(0,5):
    A.append([0]*3)
    
print(A)


def CrearMatriz(n,m):
    A=[]
    for i in range(0,n):
        A.append([0]*m)
    for i in range(0,n):
        for j in range(0,m):
            d=int(input(f'Digite Elemento ({i+1},{j+1}): '))
            A[i][j]=d
    return A

#Programa ppal

filas=int(input("Digite Número de filas: "))
columnas=int(input("Digite Número de Columnas: "))

matriz=CrearMatriz(filas, columnas)

print(matriz)

for i in matriz:
    print(i)