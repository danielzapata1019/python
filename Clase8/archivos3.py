# -*- coding: utf-8 -*-
"""
Created on Sat Apr 30 09:29:17 2022

@author: Dzapatas
"""

#Importar datos con libreria pandas

import pandas as pd

ruta='usuarios.xlsx'
data_frame=pd.read_excel(ruta)
#print(data_frame)

#con este comando puedo visualizar las n primeras filas del data frame
#print(data_frame.head(2))

#ver las n-ultimas filas del data frame
print(data_frame.tail(2))

encabezado=['primero','segundo','tercero','cuarto']
data_frame.columns=encabezado

#guardar archivo
rutag='C:\\Repositorios\\Cursos\\python\\Clase8\\usuarios2.xlsx'
data_frame.to_excel(rutag)