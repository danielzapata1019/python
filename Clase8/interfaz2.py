# -*- coding: utf-8 -*-
"""
Created on Sat Apr 30 11:47:49 2022

@author: Dzapatas
"""

import tkinter as tk
from tkinter import Label
from tkinter import Button

def mensaje ():
    print ( "Este mensaje es del boton" )


ventana = tk.Tk ()
ventana . title ( "Hola Mundo" )
ventana . geometry ( "400x200" )



lbl = Label ( ventana , text = "Esto es un label" )
lbl . config ( bg = "gray" )
lbl . pack ()



btn = Button ( ventana , text = "Presiona para ver el efecto en la terminal " , command = mensaje )
btn.config (fg = "red", bg = "blue")
#btn [ "fg" ] = "red"
#btn [ "bg" ] = "yellow"
btn . pack ()




ventana . mainloop ()