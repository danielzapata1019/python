# -*- coding: utf-8 -*-
"""
Created on Sat Apr 30 11:16:21 2022

@author: Dzapatas
"""

class equino():

    def __init__(self,alturadecruz,raza,color,paso,galopando,relinchando):
        self.alturadecruz=alturadecruz
        self.raza=raza
        self.color=color
        self.paso=paso
        self.galopando=False
        self.relinchando=False
        self.brincando=False
    
    def cabalgar(self):
        self.galopando=True
    
    def relinchar(self):
        self.relinchando=True
    
    def brincar(self):
        self.brincando=True
    
    def estado(self):
        print('altura de la cruz: ',self.alturadecruz,'\nraza: ',self.raza,'\ncolor: ',
              self.color,'\npaso: ',self.paso,'\ngalopando: ',self.galopando,
              '\nrelinchando: ',self.relinchando,'\nbrincando: ',self.brincando)

cebra=equino(1.53,'español','blanco','trotón',False,False)#aquí estoy instanciando la clase coloco el nombre del objeto=clase a la que pertenece
tormenta=equino(1.50, 'criollo','moro','trochadora',False,False)
'''
'''
cebra.estado()
#genero la clase hija de equino
class burro(equino):
    def cargar(self):
        self.cargando=True




enano=burro(1,'arabe','negro','fino',False,False)
print('el estado del enano es: ')
enano.estado()
enano.cargar()
print('enano está cargando?', enano.cargando)



'''
tormenta.cabalgar()
print('¿mi yegua tormenta está galopando?: ',tormenta.galopando)'''