# -*- coding: utf-8 -*-
"""
Created on Sat Apr 30 10:43:08 2022

@author: Dzapatas
"""

#manejo de datos en panda
#importo módulo de pandas
import pandas as pd


#importo el archivo
datos=pd.read_csv('techogar.csv',header=0)#header encabezado o primera fila

#print(datos)
#print(datos['Computadora']) #aquí miro una de las columnas
#print(datos.iloc[0:3]) #aquí miro las filas de 0 a 3
#muestro datos ordenados de manera ascendente de columna telefono
#print(datos.sort_values(by='Teléfono'))

#muestro datos de manera descendente
#print(datos.sort_values(by='Teléfono',ascending=False))

#miro los datos de la columna 5 con la condicion de ser menres a 10
#print(datos[datos.iloc[:,5]<10])
#creo una nueva lista a partir de los datos que tengo:solo la columna telefono
#tel=datos['Teléfono']
#print(tel)
#print(tel[tel>10])

################################################
#Actividades Clase
def ObtenerValorComputadoraAnio(anio):
    print ("\n")
    print ("Precio de la computadora en el año", anio, "\n")
    precioComputadora=datos.loc[(datos['Año'] == anio),["Computadora"]]
    print(precioComputadora)
    print ("\n")

def ObtenerPromedioValorTelefonoUltimosNAnios(anios):
    print ("\n")
    print("Promedio valor Teléfono en los últimos ", anios," años \n")
    anioMayor = (datos.iloc[datos['Año'].idxmax()])["Año"]
    telefonosUltimosAnios = datos.loc[(datos['Año'] > (anioMayor-anios)),["Teléfono"]]
    #print(telefonosUltimosAnios)
    print(telefonosUltimosAnios.mean())
    
def ObtenerAnioObjetoXMasCostoso(objeto):
    print ("\n")
    print ("Año en que el/la", objeto," fue más costos@ \n")
    anioTelevionCostosa=datos.iloc[datos[objeto].idxmax()]
    print(round(anioTelevionCostosa["Año"]))
    
def ObtenerDatosParaUnAnioDeterminado(anio):
    print ("Datos para el año ", anio)
    datosAnio= datos[(datos['Año']== anio)]
    print(datosAnio)

def ObtenerMayorPrecioParaXObjeto(objeto):
    #Obtener el mayor precio Internet
    #print(datos["Internet"].idxmax())
    print (objeto," de mayor precio \n")
    print(datos.iloc[datos[objeto].idxmax()])

def OrdenarDatosPorValor(valor, ascendente):
    #orden=datos[valor]    
    print(datos.sort_values(by=[valor], ascending = ascendente))


OrdenarDatosPorValor("Año", True)
ObtenerMayorPrecioParaXObjeto("Internet")
ObtenerAnioObjetoXMasCostoso("Televisión")
ObtenerValorComputadoraAnio(2004)
ObtenerPromedioValorTelefonoUltimosNAnios(5)
ObtenerDatosParaUnAnioDeterminado(2010)
