# -*- coding: utf-8 -*-
"""
Created on Sat Apr 30 10:20:55 2022

@author: Dzapatas
"""

from openpyxl import Workbook, load_workbook

libroPrueba=Workbook()

nuevaHoja=libroPrueba.create_sheet('nueva hoja')
otraHoja=libroPrueba.create_sheet('otra hoja')
otraHojaMas=libroPrueba.create_sheet('otra mas')

print(libroPrueba.sheetnames)

libroPrueba.remove(otraHojaMas)
libroPrueba.save(filename='libroPrueba.xlsx')