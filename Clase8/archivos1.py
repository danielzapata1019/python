# -*- coding: utf-8 -*-
"""
Created on Sat Apr 30 08:45:35 2022

@author: Dzapatas
"""

#Trabajo con archivos externos

archivo=open("caballos.txt","w")

archivo.write("Frisón\n")
archivo.write("Tormenta\n")

archivo.close()


#Utilizar comando with que permite escritura y lectura
with open('caballos2.txt', 'w') as archivoCaballos:
    archivoCaballos.writelines(['Goloso\n','Mechas\n','Chostoy\n'])
    
with open('caballos3.txt', 'w') as edadCaballos:
    edadCaballos.writelines(['Goloso\t 12 años \n','Mechas\t 10 años\n','Chostoy\t 15 años\n'])
    edadCaballos.close()