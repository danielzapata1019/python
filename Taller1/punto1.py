# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 23:06:03 2022

@author: Daniel Zapata
"""

print('Programa para clasificar deportistas')

nombre = str(input('Digite nombre:\n'))
sexo = int(input('Ingrese sexo(1.Masculino, 2.Femenino):\n'))
flexiones = int(input('Ingrese número de flexiones realizadas:\n'))
peso = int(input('Ingrese peso levantado en Kgs:\n'))


clasificacion=""
rangoFlexionesHombre=range(60, 100)
rangoFinalFlexionesHombre=100

rangoFlexionesMujer=range(40,80)
rangoFinalFlexionesMujer=80

rangoPesoHombre=range(60,80)
rangoFinalPesoHombre=80

rangoPesoMujer=range(30,40)
rangoFinalPesoMujer=40

if(sexo==1):
    if (flexiones in rangoFlexionesHombre and peso in rangoPesoHombre):
        clasificacion = "Optimizados"
    elif (flexiones > rangoFinalFlexionesHombre and peso > rangoFinalPesoHombre):
        clasificacion = "Potenciados"
    else :
         clasificacion = "diezmados"
   
else:
    if (flexiones in rangoFlexionesMujer and peso in rangoPesoMujer):
        clasificacion = "Optimizados"
    elif (flexiones > rangoFinalFlexionesMujer and peso > rangoFinalPesoMujer):
        clasificacion = "Potenciados"
    else :
         clasificacion = "diezmados"
        
print("Sr(ra) ", nombre, " su clasificación es: ", clasificacion)


