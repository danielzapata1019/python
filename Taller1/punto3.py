# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 23:35:19 2022

@author: Daniel Zapata
"""

print('Programa premios')

puntaje = int(input('Ingrese puntaje:\n'))

mensaje=""
premio=0

rangoDeficiente=range(0, 19)
rangoInsuficiente=range(20, 45)
rangoAceptable=range(46, 70)
rangoSobresaliente=range(71, 90)
rangoExcelente=range(91, 100)

if (puntaje in rangoDeficiente):
    mensaje = "Deficiente"
    premio=0
elif (puntaje in rangoInsuficiente):
    mensaje = "Insuficiente"
    premio=1000
elif (puntaje in rangoAceptable):
    mensaje = "Aceptable"
    premio=10000
elif (puntaje in rangoSobresaliente):
    mensaje = "Sobresaliente"
    premio=20000
elif (puntaje in rangoExcelente):
    mensaje = "Excelente"
    premio=50000
else:
    mensaje='El puntaje no está en el rango permitido'
    


print('Mensaje: ', mensaje, ' premio: ', premio)