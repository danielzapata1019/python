# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 23:12:53 2022

@author: Daniel Zapata
"""

import math
print('Tipo de triangulo')

a=float(input('Ingrese medida del lado a:\n'))
b=float(input('Ingrese medida del lado b:\n'))
c=float(input('Ingrese medida del lado c:\n'))

clasificacion=''

if(a != 0 and b!= 0 and c!=0):
    if(a >= (b+c)):
        clasificacion = 'NO FORMA UN TRIANGULO'
    elif(math.sqrt(a) == ((math.sqrt(b)) + (math.sqrt(c)))):
        clasificacion = 'el triangulo es rectángulo'
    elif(math.sqrt(a) > (math.sqrt(b)+math.sqrt(c))):
        clasificacion = 'EL TRIANGULO ES OBTUSANGULO'
    elif(math.sqrt(a) < ((math.sqrt(b)) + (math.sqrt(c)))):
        clasificacion = 'EL TRIANGULO ES ACUTANGULO'
        
        
print(clasificacion)
