# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 08:39:38 2022

@author: Daniel Zapata
"""

#Función para sumar dígitos de una cifra

def SumaDigitos(cifra):
    suma=0
    while cifra >=1:
        suma=suma+cifra%10
        cifra=cifra//10
    return suma

#Programa

numero= int(input('Digite una cifra entera: \n'))
suma = SumaDigitos(numero)
print('la suma de los dígitos es: ', suma)