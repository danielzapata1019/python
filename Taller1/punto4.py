# -*- coding: utf-8 -*-
"""
Created on Sat Mar  5 07:59:33 2022

@author: Daniel Zapata
"""

print('Programa contador digitos')

numero = int(input('Ingrese número:\n'))
contador = len(str(numero))

print('El Número: ', numero, ' tiene :', contador, ' dígitos')
