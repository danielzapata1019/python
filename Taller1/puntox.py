# -*- coding: utf-8 -*-
"""
Created on Sat Mar  5 08:15:11 2022

@author: Daniel Zapata
"""

#División por restas sucesivas

dividendo=int(input('Ingrese el dividendo:\n'))
divisor=int(input('Ingrese el divisor:\n'))
contador=0

residuo = dividendo

while residuo > divisor:
    residuo = residuo - divisor
    contador =  contador + 1 
    
print('El cociente es: ', contador)
print('El residuo es: ', residuo)