# -*- coding: utf-8 -*-
"""
Created on Fri Mar 25 22:16:32 2022

@author: Daniel Zapata
"""

print('Creación de un diccionario de manera modular')

def CrearDicc(numeroElementos):
    diccionario={}
    
    for i in range(numeroElementos):
        clave= input('Digite Nombre: \n')
        valor=float(input('Ingrese saldo cliente: \n'))
        diccionario[i]=dict({clave:valor})
    return diccionario


#programa ppal

libreta={}
x=int(input('Digite la cantidad de pares del diccionario que quiere crear: \n'))

libreta=CrearDicc(x)

print('Su diccionario es: ', libreta)
