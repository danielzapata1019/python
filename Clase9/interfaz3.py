# -*- coding: utf-8 -*-
"""
Created on Sat May  7 10:17:08 2022

@author: daniel.zapata2
"""

import tkinter as tk
from tkinter import messagebox

def Validar():
    if entrada1.get()=='Python':
        AbrirVentana2()
    else:
        messagebox.showwarning('Advertencia','Password no corresponde')
    
def AbrirVentana2():
    ventana.withdraw()
    win=tk.Toplevel()
    win.geometry('380x300')
    win.configure(bg='gray')
    win.title('Ventana 2')
    e2=tk.Label(win, text='Bienvenido al curso de Python', bg='gray', fg='white')
    e2.pack(padx=5, pady=5,ipadx=5, ipady=5, fill=tk.Y)
    boton2=tk.Button(win, text='Ok', command=win.destroy)
    boton2.pack()
    
ventana=tk.Tk()
ventana.title('Ventana en Python')
#raiz.config(width=600, height=400)
ventana.geometry('380x300')
ventana.configure(bg='gray')
ventana.iconbitmap('Python.ico')
ventana.resizable(0,0)

#Crear etiquetas(label)
e1=tk.Label(ventana,text='Password: ', bg='gray', fg='white')
e1.pack(padx=10, pady=50,ipadx=10, ipady=10)
#caja de texto textbox
entrada1=tk.Entry(ventana)
entrada1.pack(fill=tk.Y, padx=5, pady=5, ipady=5, ipadx=5)

boton=tk.Button(ventana, text='Validar password', 
                bg='darkblue', fg='white', command=Validar)
boton.pack()
ventana.mainloop()