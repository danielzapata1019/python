# -*- coding: utf-8 -*-
"""
Created on Sat May  7 11:48:03 2022

@author: daniel.zapata2
"""

import tkinter as tk

raiz=tk.Tk()
raiz.title('Suma de dos números')
raiz.geometry('380x300')
raiz.configure(bg='dark turquoise')
#aqui creo una función para sumar los números ingresados
def suma():
    s=int(entrada1.get()) + int(entrada2.get())
    return variable.set(s)


def cerrar():
    raiz.destroy()

variable=tk.StringVar()#sirve de enlace entre el método y la etiqueta
#guarda el resultado
#creo la etiqueta 1
etiq1=tk.Label(raiz,text='Ingrese número 1',bg='red',fg='black')
#dimensiono de la etiqueta 1
etiq1.pack(padx=10,pady=10,ipadx=10,ipady=10,fill=tk.X)
# genero espacio para ingresar el número 1, primero lo creo y luego configuro
entrada1=tk.Entry(raiz)
entrada1.pack(fill=tk.X,padx=5,pady=5,ipadx=5,ipady=5)
#creo etiqueta 2
etiq2=tk.Label(raiz,text='Ingrese número 2',bg='red',fg='black')
#dimensiono de la etiqueta 2
etiq2.pack(padx=5,pady=5,ipadx=5,ipady=5,fill=tk.X)
# genero espacio para ingresar el número 2, primero lo creo y luego configuro
entrada2=tk.Entry(raiz)
entrada2.pack(fill=tk.X,padx=5,pady=5,ipadx=5,ipady=5)
#aquí creo el boton que cuando pulso suma
botondesuma=tk.Button(raiz,text='Sumar',fg='blue',command=suma)
botondesuma.pack(side=tk.TOP)



#aquí está la etiqueta para ver el resultado
resultado=tk.Label(raiz,bg='plum',textvariable=variable,padx=5,pady=5,width=5)
resultado.pack()


#aquí está el boton para cerrar la ventana
botoncerrar=tk.Button(raiz,text='Pulse aquí para cerrar la aplicación',fg='blue',command=cerrar)
botoncerrar.pack(side=tk.TOP)


raiz.mainloop()