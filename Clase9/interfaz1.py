# -*- coding: utf-8 -*-
"""
Created on Sat May  7 08:48:22 2022

@author: daniel.zapata2
"""

import tkinter as tk
from tkinter import Label
from tkinter import Button


raiz=tk.Tk()
raiz.title('Primera ventana en Python')
#raiz.config(width=600, height=400)
raiz.geometry('800x500')
raiz.configure(bg='gray')
raiz.iconbitmap('Python.ico')
raiz.resizable(0,0)

etiqueta=Label(raiz, text='esto es una etiqueta')
raiz.mainloop()