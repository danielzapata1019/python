# -*- coding: utf-8 -*-
"""
Created on Thu May 12 21:45:31 2022

@author: Daniel Zapata
"""

#Tarea Interfaz - Operaciones Matematicas

import tkinter as tk
from tkinter import messagebox
from PIL import ImageTk, Image

bgColorFondo='#DCDFE3'
bgBotones='#17437D'
fgBotones='white'

ventana=tk.Tk()
ventana.title('Operaciones en Python')
#raiz.config(width=600, height=400)
ventana.geometry('600x400')
ventana.configure(bg=bgColorFondo)
ventana.iconbitmap('Python.ico')
ventana.resizable(0,0)

def Limpiar():
    resultado=0
    txtNumero1.delete(0, tk.END)
    txtNumero2.delete(0, tk.END)
    return variable.set(resultado)

def Validar():
    numero1 = txtNumero1.get()
    numero2 = txtNumero2.get()
    if len(numero1)==0 or len(numero2)==0:
       messagebox.showwarning('Advertencia','Debe ingresar un número en los campos')
       
def ValidarDivision():    
    numero2 = txtNumero2.get()
    if float(numero2)==0:
       messagebox.showwarning('Advertencia','No se puede dividir por 0')
        
def Sumar():
    Validar()
    resultado=float(txtNumero1.get()) + float(txtNumero2.get())
    return variable.set(resultado)

def Restar():
    Validar()
    resultado=abs(float(txtNumero1.get()) - float(txtNumero2.get()))
    return variable.set(resultado)

def Multiplicar():
    Validar()
    resultado=float(txtNumero1.get()) * float(txtNumero2.get())
    return variable.set(resultado)

def Dividir():
    Validar()
    ValidarDivision()
    resultado=float(txtNumero1.get()) / float(txtNumero2.get())
    return variable.set(resultado)
    
def DivisionEntera():
    Validar()
    ValidarDivision()
    resultado=float(txtNumero1.get()) // float(txtNumero2.get())
    return variable.set(resultado)

def Modulo():
    Validar()
    resultado=float(txtNumero1.get()) % float(txtNumero2.get())
    return variable.set(resultado)

def Potencia():
    Validar()
    resultado=float(txtNumero1.get()) ** float(txtNumero2.get())
    return variable.set(resultado)

def Cociente():
    Validar()
    contador=0
    residuo = float(txtNumero1.get())
    divisor = float(txtNumero2.get())
    
    while residuo > divisor:
        residuo = residuo - divisor
        contador =  contador + 1 
    return variable.set(contador)
   
    
def Residuo():
    Validar()
    contador=0
    residuo = float(txtNumero1.get())
    divisor = float(txtNumero2.get())
    
    while residuo > divisor:
        residuo = residuo - divisor
        contador =  contador + 1 
    
    return variable.set(residuo)

variable=tk.StringVar()
lblTitulo=tk.Label(ventana,text='Operaciones en Python - Curso UDEM',
                   bg=bgColorFondo,fg='black', font='Arial 10 bold')
lblTitulo.pack()

lblNumero1=tk.Label(ventana,text='Número 1:',
                    bg=bgColorFondo,fg='black').place(x=30, y=40)

txtNumero1=tk.Entry(ventana)
txtNumero1.place(x=100, y=40, width=130, height=25)

lblNumero2=tk.Label(ventana,text='Número 2:',
                    bg=bgColorFondo,fg='black').place(x=290, y=40)
txtNumero2=tk.Entry(ventana)
txtNumero2.place(x=360, y=40, width=130, height=25)

lblResultado=tk.Label(ventana,text='Resultado:',
                    bg=bgColorFondo,fg='black').place(x=30, y=80)

txtResultado=tk.Entry(ventana, text=variable, state='disabled')
txtResultado.place(x=100, y=80, width=330, height=25)

btnLimpiar=tk.Button(ventana,text='Limpiar',fg='black', bg='#3C7FFC',command=Limpiar)
btnLimpiar.place(x=440, y=80)

lblOPeraciones=tk.Label(ventana,text='Operaciones',
                   bg=bgColorFondo,fg='black', font='Arial 10').place(x=250, y=150)

btnSumar=tk.Button(ventana,text='Sumar',fg=fgBotones,bg=bgBotones,command=Sumar)
btnSumar.place(x=30, y=250, width=80)

btnRestar=tk.Button(ventana,text='Restar',fg=fgBotones,bg=bgBotones,command=Restar)
btnRestar.place(x=120, y=250, width=80)

btnMultiplicar=tk.Button(ventana,text='Multiplicar',fg=fgBotones,bg=bgBotones,command=Multiplicar)
btnMultiplicar.place(x=210, y=250, width=80)

btnDividir=tk.Button(ventana,text='Dividir',fg=fgBotones,bg=bgBotones,command=Dividir)
btnDividir.place(x=300, y=250, width=80)

btnDividirEntera=tk.Button(ventana,text='División Entera',
                           fg=fgBotones,bg=bgBotones,command=DivisionEntera)
btnDividirEntera.place(x=30, y=300, width=80)

btnModulo=tk.Button(ventana,text='Modulo',fg=fgBotones,bg=bgBotones,command=Modulo)
btnModulo.place(x=120, y=300, width=80)

btnPotencia=tk.Button(ventana,text='Potencia',fg=fgBotones,bg=bgBotones,command=Potencia)
btnPotencia.place(x=210, y=300, width=80)

btnCociente=tk.Button(ventana,text='Cociente',fg=fgBotones,bg=bgBotones,command=Cociente)
btnCociente.place(x=300, y=300, width=80)

btnResiduo=tk.Button(ventana,text='Residuo',fg=fgBotones,bg=bgBotones,command=Residuo)
btnResiduo.place(x=30, y=350, width=80)

image=Image.open('UdeM.png')
img=image.resize((120, 120), Image.ANTIALIAS)
imagen = ImageTk.PhotoImage(img)
lblImagen = tk.Label(ventana, image = imagen, bg=bgColorFondo)
lblImagen.place(x=460,y=270, width=120, height=120)

ventana.mainloop()