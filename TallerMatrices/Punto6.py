# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 08:50:46 2022

@author: Daniel Zapata
"""

def CrearVector(rango):
    vectorA=[None]*rango
    for i in range(rango):       
        if i%2==0:
            vectorA[i]=1
        else:
            vectorA[i]=0
            
    return vectorA

rango=int(input('Ingrese rango: \n'))
A=CrearVector(rango)

print(A)