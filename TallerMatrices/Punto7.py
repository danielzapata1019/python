# -*- coding: utf-8 -*-
"""
Created on Sat Mar 12 09:07:16 2022

@author: Daniel Zapata
"""

#Punto 7 vectores y matrices
import random

def CrearVectoresPersonas(rango):
    vectorPersonas=[None]*rango
    
    for i in range(rango):
        elemento=input('Digite nombre de la persona:\n')
        vectorPersonas[i]=elemento        
    return vectorPersonas

def CrearVectoresEdades(rango):   
    vectorEdades=[None]*rango    
    
    for i in range(rango):        
        vectorEdades[i]=random.randint(15, 90)
    return vectorEdades

def CrearVectoresEstaturas(rango):    
    vectorEstaturas=[None]*rango
    
    for i in range(rango):        
        vectorEstaturas[i]=random.randint(100, 200)
    return vectorEstaturas


def ObtenerResultados(personas, edades, estaturas):
    jovenes= 200
    altos=0
    personaJoven=''
    personaAlta=''

    for i in range(len(personas)):
        if edades[i]< jovenes:
            jovenes= edades[i]
            personaJoven = personas[i]
        if estaturas[i] > altos:
            altos= estaturas[i]
            personaAlta = personas[i]
    
    return personaJoven, personaAlta


#Programa
rango = int(input('Ingrese Rango: \n'))
personas = CrearVectoresPersonas(rango)
edades = CrearVectoresEdades(rango)
estaturas = CrearVectoresEstaturas(rango)

print ('Los nombres son: ', personas)
print ('Las edades son: ', edades)
print ('Las estaturas son: ', estaturas)

#resultado =  ObtenerResultados(personas, edades, estaturas)
joven, alta = ObtenerResultados(personas, edades, estaturas)
print('Persona mas Joven: ', joven)
print('Persona mas Alta: ', alta)



print('La persona más joven es: ', personas[edades.index(min(edades))])
print('La persona más alta es: ', personas[estaturas.index(max(estaturas))])