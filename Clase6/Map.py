# -*- coding: utf-8 -*-
"""
Created on Sat Apr  2 09:44:08 2022

@author: Daniel Zapata
"""

A=list(range(5,100,10))
print(A)

mult3=list(map(lambda x:x**3, A))
print(mult3)

f= lambda x,y=2:x*y
print(list(map(f,[2,3,4])))