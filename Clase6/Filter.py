# -*- coding: utf-8 -*-
"""
Created on Sat Apr  2 09:24:19 2022

@author: Daniel Zapata
"""

A=list(range(5,100,10))
print(A)

list1=list(filter(lambda x:x%3 !=0, A))
print(list1)

list2=list(filter(lambda x:x>50 and x<80, A))
print(list2)