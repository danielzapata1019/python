# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 20:56:42 2022

@author: Daniel Zapata
"""

def DivisionReal(n,m):
    try:
        r=n/m
        return r
    except ZeroDivisionError:
        print("No se puede dividir por cero!!")
        

def DivisionEntera(n,m):
    try:
        r=n//m
        return r
    except ZeroDivisionError:
        print("No se puede dividir por cero!!")
        
def ResiduoDivision(n,m):
    try:
        r=n%m
        return r
    except ZeroDivisionError:
            print("No se puede aplicar modulo con cero!!")


#Programa Ppal

while True:
    try:
        n=float(input('Digite dividendo \n'))
        break
    except ValueError:
        print("El valor introducido es erróneo!!")
        
while True:
    try:
        m=float(input('Digite divisor \n'))
        break
    except ValueError:
        print("El valor introducido es erróneo!!")
        
real= DivisionReal(n, m)
print("El cociente real es: ", real)

entera =  DivisionEntera(n, m)
print("El cociente división entera es: ", entera)

modulo=ResiduoDivision(n, m)
print("El módulo de los números es: ", modulo)

print("Programa terminado")
