# -*- coding: utf-8 -*-
"""
Created on Sat Feb 26 11:54:01 2022

@author: Daniel Zapata
"""
print('Ciclo While')

i = 1
while i < 21:
  if i%2==0:
      print(i)
  i += 1