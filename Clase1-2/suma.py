# -*- coding: utf-8 -*-
"""
Created on Sat Feb 26 10:39:04 2022

@author: Daniel Zapata
"""

print('Programa para sumar 2 números')

numero1 = float(input('Digite primer número:\n'))
numero2 = float(input('Digite segundo número:\n'))

resultado=numero1+numero2

print('El resultado es: ', resultado)