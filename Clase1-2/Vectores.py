# -*- coding: utf-8 -*-
"""
Created on Sat Mar  5 09:28:38 2022

@author: Daniel Zapata
"""

#Creación de un vector con una función

def CrearVector(rango):
    vectorA=[None]*rango
    for i in range(rango):
        elemento=float(input('Digite el elemento del vector:\n'))
        vectorA[i]=elemento
    return vectorA

def CalcularPromedio(vector, rango):
    suma=0
    for i in range(rango):
        suma = suma + vector[i]
        
    promedio = suma/rango
    return promedio

def CalcularMaximo(vector):
    maximo=vector[1]
    
    for i in range(len(vector)):
        if(vector[i]>maximo):
            maximo=vector[i]
    return maximo

rango=int(input('Ingrese rango del vector'))

A=CrearVector(rango)

print(A)

for i in A:
    print(i)
    
    
prom=CalcularPromedio(A,rango)

print('El promedio de los elementos es: ', prom)
  
maximo=CalcularMaximo(A)
print('El valor máximo del vector es: ', maximo)
