# -*- coding: utf-8 -*-
"""
Created on Sat Feb 26 10:55:58 2022

@author: Daniel Zapata
"""
import math
print('Área de un triangulo')

a=float(input('Ingrese medida del lado a:\n'))
b=float(input('Ingrese medida del lado b:\n'))
c=float(input('Ingrese medida del lado c:\n'))

p=(a+b+c)/2

#print(p)

#area=(p*(p-a)*(p-b)*(p-c))**(1/2)


if(p-a)>0 and (p-b)>0 and (p-c)>0:
    area=math.sqrt((p*(p-a)*(p-b)*(p-c)))    
    print('El área del trianguilo es: ', area)
else:
    print('las medidas no forman un triangulo')
    
print('fin programa')
