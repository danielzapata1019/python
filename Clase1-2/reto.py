# -*- coding: utf-8 -*-
"""
Created on Sat Mar  5 09:05:19 2022

@author: Daniel Zapata
"""

#reto

def CalcularCociente(divid, divis):
    contador=0
    residuo = divid
    
    while residuo > divis:
        residuo = residuo - divis
        contador =  contador + 1 
    
    return contador, residuo

dividendo=int(input('Ingrese el dividendo:\n'))
divisor=int(input('Ingrese el divisor:\n'))
   
c,r=CalcularCociente(dividendo, divisor)

print('El cociente es: ', c)
print('El residuo es: ', r)


