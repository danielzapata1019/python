# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 11:06:48 2022

@author: Daniel Zapata
"""
#Concesionario - Tarea Clase - Casa

class Vehiculo():
    def __init__(self, marca, modelo, anio, placa, kilometraje, color):
        self.marca=marca
        self.modelo=modelo
        self.anio=anio
        self.placa=placa        
        self.kilometraje=kilometraje
        self.color = color
        self.vendido = False
        
    def VenderVehiculo(self, vender):
        self.vendido=vender
    
    
class Persona():
    def __init__(self, nombre, apellido, profesion, residencia, carro):
        self.nombre = nombre
        self.apellido= apellido
        self.profesion= profesion
        self.residencia= residencia
        self.carro:Vehiculo = carro


def ObtenerInformacionCliente(cliente:Persona):
    print("********** Información Persona **********")
    print("Nombre: ", cliente.nombre)
    print("Apellido: ", cliente.apellido)
    print("Profesión: ", cliente.profesion)
    print("Residencia: ", cliente.residencia)
    print("\n Información Vehiculo: \n")
    print("Marca: ", cliente.carro.marca)
    print("Modelo: ", cliente.carro.modelo)
    print("Color: ", cliente.carro.color)
    print("Año: ", cliente.carro.anio)
    print("Kilometraje: ", cliente.carro.kilometraje)
    print("Placa: ", cliente.carro.placa)
    print("Vendido: ", cliente.carro.vendido, "\n")
    
carroDaniel=Vehiculo("Mazda", "2 sedan", 2021, "JJJ123", 100, "gris asabache")
carroDaniel.VenderVehiculo(True)
daniel=Persona("Daniel","Zapata", "Ingeniero de Sistemas", "Santa Elena", carroDaniel)

carroJuliana=Vehiculo("Renault", "Logan", 2018, "PQR987", 250, "Blanco")
juliana=Persona("Juliana","Soto", "Ingeniera Ambiental", "El Carmen Viboral", carroJuliana)

ObtenerInformacionCliente(daniel)
ObtenerInformacionCliente(juliana)

