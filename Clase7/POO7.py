# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 10:44:31 2022

@author: Dzapatas
"""

class FichaEmpleado():
    def __init__(self):
        self.nombre=None
        self.edad=None
        self.id=None
        
    def ObtenerSalario(self, horaTrabajada, tarifa):
        self.salario = horaTrabajada * tarifa
        return self.salario
    
    def CalcularDescuentos(self):
        return self.salario * 0.20
    

def IniciarPrograma():
    a=FichaEmpleado()
    a.nombre="Daniel Zapata"
    a.edad=30
    a.id=123
    a.horasTrabajadas = 30
    a.tarifa=200
    
    b=FichaEmpleado()
    b.nombre="Juan Martinez"
    b.edad=35
    b.id=559
    b.horasTrabajadas = 20
    b.tarifa=150
        
    c=FichaEmpleado()
    c.nombre="Gabriel Garcia"
    c.edad=60
    c.id=111
    c.horasTrabajadas = 35
    c.tarifa=250
    
    
    print(b.nombre)
    
    sueldo=a.ObtenerSalario(a.horasTrabajadas, a.tarifa)
    print("El salario de ", a.nombre, "es: ", sueldo)

IniciarPrograma()
    