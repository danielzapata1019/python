# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 08:25:03 2022

@author: Dzapatas
"""

class Persona():
    nombre="Carlos"
    apellido="Montoya"
    profesion="Abogado"
    residencia="Guarne"
    contrasena="12345"
    
    

#Ejemplerización de la clase
a=Persona()
#Acceso a las caracteristicas con la notación punto
print(a.apellido)
print(a.residencia)
print(a.contrasena)
a.apellido="Ruiz"
print(a.apellido)