# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 09:26:00 2022

@author: Dzapatas
"""

class Equino():
    def __init__(self, nombre,alturaCruz, raza, color, paso, cabalgando, relinchando, brincando):        
        self.nombre = nombre
        self.alturaCruz = alturaCruz
        self.raza = raza
        self.color = color
        self.paso = paso
        self.cabalgando = cabalgando
        self.relinchando =relinchando
        self.brincando =  brincando
    
    def Cabalgar(self):
        self.cabalgando = True
        
    def Relinchar(self):
        self.relinchando = True
        
    def Brincar(self):
        self.brincando =  True
        
    def ObtenerEstado(self):        
        if self.cabalgando:
            print(self.nombre, " Esta cabalgando")
        else:
            print(self.nombre, " No esta cabalgando")
        if self.relinchando:
            print(self.nombre, " Esta relinchando")
        else:
            print(self.nombre, " No esta relinchando")
        if self.brincando:
            print(self.nombre, " Esta agitado")
        else:
            print(self.nombre, " Esta relajado")
            
cebra=Equino("Cebra", 1.5, "Criollo", "Negro", "Fino", True, False, False)
tormenta = Equino("Tormenta", 2, "Española", "Moro", "Trochadora", False, True, False)

print(tormenta.nombre)
print(tormenta.raza)
tormenta.brincando=True
print(tormenta.brincando)
tormenta.ObtenerEstado()
