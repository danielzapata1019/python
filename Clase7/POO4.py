# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 09:13:03 2022

@author: Dzapatas
"""

class Equino():
    nombre = "cebra"
    alturaCruz = 1.4
    raza = "Criollo"
    color = "Cafe"
    paso = "Fino"
    cabalgando = False
    relinchando = False
    brincando =  False
    
    def Cabalgar(self):
        self.cabalgando = True
        
    def Relinchar(self):
        self.relinchando = True
        
    def Brincar(self):
        self.brincando =  True
        
    def ObtenerEstado(self):        
        if self.cabalgando:
            print(self.nombre, " Esta cabalgando")
        else:
            print(self.nombre, " No esta cabalgando")
        if self.relinchando:
            print(self.nombre, " Esta relinchando")
        else:
            print(self.nombre, " No esta relinchando")
        if self.brincando:
            print(self.nombre, " Esta agitado")
        else:
            print(self.nombre, " Esta relajado")
            
cebra=Equino()

print("La raza de ", cebra.nombre, "es ", cebra.raza)
cebra.cabalgando=True
print("El estado de ", cebra.nombre, "es ")
cebra.ObtenerEstado()

    