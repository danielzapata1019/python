# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 08:52:14 2022

@author: Dzapatas
"""

class Persona():
    nombre="Carlos"
    apellido="Montoya"
    profesion="Abogado"
    residencia="Guarne"    
    trabajando = False
    hablando =  False
    #Crear Métodos
    
    def Trabajar(self):
        self.trabajando =  True
    
    def Hablar(self):
        self.hablando =  True
        
        
P1=Persona()
#print(P1.residencia)

P2=Persona()
P2.nombre="Daniel"
P2.apellido="Zapata"
P2.profesion="Ingeniero de Sistemas"
P2.residencia="Medellín"
P2.trabajando=True
P2.hablando=False

print(P2.nombre)