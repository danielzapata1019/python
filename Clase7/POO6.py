# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 10:17:08 2022

@author: Dzapatas
"""

class Caja():
    def __init__(self, largo, ancho, costo):
        self.largo = largo
        self.ancho = ancho
        self.costo = costo
        
        
    def CalcularPerimetro(self):
        return 2*(self.largo+self.ancho)
    
    def CalcularArea(self):
        return self.ancho*self.largo
    
    def CalcularPrecio(self):
        area=self.CalcularArea()
        return area*self.costo
        
print("Ingrese las medidas de la caja(Largo y Ancho)")
largo=float(input("Ingrese el largo \n"))
ancho=float(input("Ingrese el ancho \n"))
costo=float(input("Ingrese costo unitario del material \n"))

A=Caja(largo, ancho, costo)

print("El Area de la caja es: ", A.CalcularArea())
print("El material para la caja cuesta: ", A.CalcularPrecio())